

## faire un lien symbolique vers le fichier manifest.json

```
ln -s front/build/manifest.json php/public/build/manifest.json
```


## Diviser un fichier csv en bash

```
tail -n +2 CMB2012.csv | split -l 1000 - split_
for file in split_*
do
    head -n 1 CMB2012.csv > tmp_file
    cat $file >> tmp_file
    mv -f tmp_file $file
done
```
