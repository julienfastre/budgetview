<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Entity\CodeInterface;
use App\Entity\Budget;
use App\Calculator\ByCodeCalculator;
use App\Entity\FunctionalCode;
use App\Entity\EconomicalCode;

class AppCodeExtension extends AbstractExtension
{
    /**
     *
     * @var ByCodeCalculator
     */
    protected $byCodeCalculator;
    
    function __construct(ByCodeCalculator $byCodeCalculator)
    {
        $this->byCodeCalculator = $byCodeCalculator;
    }

    
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
           //new TwigFilter('sum_articles', [$this, 'getSumAmount']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('code_ancestors', [$this, 'getCodeAncestors']),
            new TwigFunction('sum_articles', [$this, 'getSumAmountArticle']),
            new TwigFunction('count_articles', [$this, 'getCountArticle']),
            new TwigFunction('function_repartition_data_js', [$this, 'getByFunctionRepartitionJS']),
            new TwigFunction('economical_repartition_data_js', [$this, 'getByEconomicalRepartitionJS'])
        ];
    }

    public function getCodeAncestors(CodeInterface $code)
    {
        return $this->getParentsRecursive($code, [ $code->getId() => $code]);
    }
    
    private function getParentsRecursive(CodeInterface $code, array $tree)
    {
        if ($code->hasParent()) {
            $parent = $code->getParent();
            
            return $this->getParentsRecursive($parent, \array_merge([ $parent->getId() => $parent], $tree));
        }
        
        return $tree;
    }
    
    public function getSumAmountArticle(FunctionalCode $functionalCode, EconomicalCode $economicalCode, Budget $budget, $expensesOrRevenues)
    {
        return $this->byCodeCalculator->sumAmountByCode($functionalCode, $economicalCode,  $budget, $expensesOrRevenues);
    }
    
    public function getCountArticle(FunctionalCode $functionalCode, EconomicalCode $economicalCode, Budget $budget, $expensesOrRevenues)
    {
        return $this->byCodeCalculator->countArticlesByCode($functionalCode, $economicalCode,  $budget, $expensesOrRevenues);
    }
    
    public function getByFunctionRepartitionJS(FunctionalCode $functionalCode, EconomicalCode $economicalCode, Budget $budget)
    {
        $labels      = [];
        $sumExpenses = [];
        $sumRevenues = [];
        
        foreach ($functionalCode->getChildren() as $code) {
            $e = $this->byCodeCalculator
                ->sumAmountByCode($code, $economicalCode, $budget, 'expenses');
            $r = $this->byCodeCalculator
                ->sumAmountByCode($code, $economicalCode, $budget, 'revenues');
            
            if ($r > 0.0 or $e > 0.0) {
            
                $labels[]        = (string) $code;
                $sumExpenses[]   = $e;
                $sumRevenues[]   = $r;
            }
        }
        
        return \json_encode([
            'labels' => $labels,
            'sumExpenses' => $sumExpenses,
            'sumRevenues' => $sumRevenues
        ]);
    }
    
    public function getByEconomicalRepartitionJS(FunctionalCode $functionalCode, EconomicalCode $economicalCode, Budget $budget, $expensesOrRevenues)
    {
        $labels      = [];
        $sum = [];
        
        foreach ($economicalCode->getChildren() as $code) {
            $s = $this->byCodeCalculator
                ->sumAmountByCode($functionalCode, $code, $budget, $expensesOrRevenues);
            
            if ($s > 0.0) {
                $labels[]        = (string) $code;
                $sum[] = $s;
            }
        }
        
        return \json_encode([
            'labels' => $labels,
            'sum' => $sum,
        ]);
    }
}
