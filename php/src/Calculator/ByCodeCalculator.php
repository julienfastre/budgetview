<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace App\Calculator;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\CodeInterface;
use App\Entity\Budget;
use App\Entity\FunctionalCode;
use App\Entity\EconomicalCode;
use App\Entity\Article;

/**
 * 
 *
 */
class ByCodeCalculator
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    
    /**
     * 
     * @param CodeInterface $code
     * @param Budget $budget
     * @param string $expensesOrRevenues 'expenses' or 'revenues'
     * @return float
     */
    public function sumAmountByCode(FunctionalCode $functionalCode, EconomicalCode $economicalCode, Budget $budget, $expensesOrRevenues): float
    {
        $qb = $this->buildQueryArticlesByCode($functionalCode, $economicalCode, $budget, $expensesOrRevenues);
        
        $qb
            ->select('SUM(a.amount)')
            ;
        
        return $qb->getQuery()->getSingleScalarResult() ?? 0;
    }
    
        /**
     * 
     * @param CodeInterface $code
     * @param Budget $budget
     * @param string $expensesOrRevenues 'expenses' or 'revenues'
     * @return int
     */
    public function countArticlesByCode(FunctionalCode $functionalCode, EconomicalCode $economicalCode, Budget $budget, $expensesOrRevenues): float
    {
        $qb = $this->buildQueryArticlesByCode($functionalCode, $economicalCode, $budget, $expensesOrRevenues);
        
        $qb
            ->select('COUNT(a)')
            ;
        
        return $qb->getQuery()->getSingleScalarResult() ?? 0;
    }
    
    protected function buildQueryArticlesByCode(
        FunctionalCode $functionalCode, 
        EconomicalCode $economicalCode, 
        Budget $budget, 
        $expensesOrRevenues
    ) : \Doctrine\ORM\QueryBuilder
    {
        $childrenEconomicalCodeIds = $this->em->getRepository(EconomicalCode::class)
            ->findByRoot($economicalCode);
        $childrenFunctionalCodeIds = $this->em->getRepository(FunctionalCode::class)
            ->findByRoot($functionalCode);
        
        
        $qb = $this->em->createQueryBuilder();
        $qb
            ->from(Article::class, 'a')
            ->where($qb->expr()->eq('a.budget', ':budget'))
            ->setParameter('budget', $budget)
            ->andWhere($qb->expr()->eq('a.isExpense', ':isExpense'))
            ->setParameter('isExpense', $expensesOrRevenues === 'expenses')
            ->andWhere($qb->expr()->in('a.economicalCode', ':codesEconomical'))
            ->andWhere($qb->expr()->in('a.functionalCode', ':codesFunctional'))
            ->setParameter('codesEconomical', $childrenEconomicalCodeIds)
            ->setParameter('codesFunctional', $childrenFunctionalCodeIds)
            ;
        
        return $qb;
    }
    
    private function getRepository(CodeInterface $code)
    {
        if ($code instanceof EconomicalCode) {
            return $this->em->getRepository(EconomicalCode::class);
        } elseif ($code instanceof FunctionalCode) {
            return $this->em->getRepository(FunctionalCode::class);
        } else {
            throw new \OutOfBoundsException("This class is not implemented");
        }
    }
}
