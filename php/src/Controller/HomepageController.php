<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Entity;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $entities = $this->getDoctrine()->getManager()
            ->getRepository(Entity::class)
            ->findAll()
            ;
        
        return $this->render('homepage/index.html.twig', [
            'entities' => $entities
        ]);
    }
}
