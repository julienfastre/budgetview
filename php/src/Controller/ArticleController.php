<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use App\Entity\Budget;
use App\Entity\Entity as BudgetEntity;
use App\Entity\FunctionalCode;
use App\Entity\EconomicalCode;
use App\Entity\Article;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends AbstractController
{
    /**
     *
     * @var SerializerInterface
     */
    protected $serializer;
    
    function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/article/{entity_slug}/{year}/{version}/{functionalCode_id}/{economicalCode_id}/{expensesOrRevenues}/{ordinaryOrExtraordinary}.{_format}", 
     * name="articles_by_code",
     * requirements= {
     *  "_format": "json",
     *  "expensesOrRevenues": "expenses|revenues|both",
     *  "ordinaryOrExtraordinary": "ordinary|extraordinary|both"
     * })
     * @Entity("entity", expr="repository.findOneBySlug(entity_slug)")
     * @ParamConverter("functionalCode", options={"id" = "functionalCode_id"})
     * @ParamConverter("economicalCode", options={"id" = "economicalCode_id"})
     */
    public function index(
        BudgetEntity $entity, 
        $year, $version,
        FunctionalCode $functionalCode, 
        EconomicalCode $economicalCode,
        $expensesOrRevenues,
        $ordinaryOrExtraordinary,
        Request $request
    ) {
        $em = $this->getDoctrine()->getManager();
        
        $budget = $em->getRepository(Budget::class)
            ->findOneBy([
                'entity' => $entity,
                'year' => $year,
                'version' => $version
            ]);
        
        if (NULL === $budget) {
            throw $this->createNotFoundException("Budget not found");
        }
        
        $articles = $this->getDoctrine()->getRepository(Article::class)
            ->findByWithRecursiveCode([
                'budget' => $budget,
                'functionalCode' => $functionalCode,
                'economicalCode' => $economicalCode,
                'isExpense' => $this->parseExpensesOrRevenues($expensesOrRevenues),
                'isOrdinary' => $this->parseOrdinaryOrExtraordinary($ordinaryOrExtraordinary)
            ], ['amount' => 'DESC'], 
            $request->query->get('limit', 50),
            $request->query->get('offset', 0));
        
        return new Response($this->serializer->serialize($articles, 'json'));
    }
    
    protected function parseExpensesOrRevenues($expensesOrRevenues) 
    {
        switch ($expensesOrRevenues) {
            case 'expenses': return true;
            case 'revenues': return false;
            case 'both': return null;
        }
    }
    
    protected function parseOrdinaryOrExtraordinary($ordinaryOrExtraordinary)
    {
        switch ($ordinaryOrExtraordinary) {
            case 'ordinary': return true;
            case 'extraordinary': return false;
            case 'both': return null;
        }
    }
}
