<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use App\Entity\Budget;
use App\Entity\Entity as BudgetEntity;
use App\Entity\FunctionalCode;
use App\Entity\EconomicalCode;
use App\Entity\Article;

class BudgetByFunctionController extends AbstractController
{
    /**
     * @Route("/budget/by/function/{entity_slug}/{year}/{version}/{functionalCode_id}/{economicalCode_id}", name="budget_by_function")
     * @Entity("entity", expr="repository.findOneBySlug(entity_slug)")
     * @ParamConverter("functionalCode", options={"id" = "functionalCode_id"})
     * @ParamConverter("economicalCode", options={"id" = "economicalCode_id"})
     */
    public function index( 
        BudgetEntity $entity, 
        $year, $version,
        FunctionalCode $functionalCode, 
        EconomicalCode $economicalCode
    ) {
        
        $em = $this->getDoctrine()->getManager();
        
        $budget = $em->getRepository(Budget::class)
            ->findOneBy([
                'entity' => $entity,
                'year' => $year,
                'version' => $version
            ]);
        
        if (NULL === $budget) {
            throw $this->createNotFoundException("Budget not found");
        }
        
        return $this->render('budget_by_function/index.html.twig', [
            'budget' => $budget,
            'functionalCode' => $functionalCode,
            'economicalCode' => $economicalCode,
            'entity' => $entity,
            'numArticlesExpensesOrdinary' => $em->getRepository(Article::class)
                ->countWithRecursiveCode([
                    'budget' => $budget,
                    'functionalCode' => $functionalCode,
                    'economicalCode' => $economicalCode,
                    'isExpense' => true,
                    'isOrdinary' => true
            ]),
            'numArticlesRevenuesOrdinary' => $em->getRepository(Article::class)
                ->countWithRecursiveCode([
                    'budget' => $budget,
                    'functionalCode' => $functionalCode,
                    'economicalCode' => $economicalCode,
                    'isExpense' => false,
                    'isOrdinary' => true
            ]),
            'numArticlesExpensesExtraordinary' => $em->getRepository(Article::class)
                ->countWithRecursiveCode([
                    'budget' => $budget,
                    'functionalCode' => $functionalCode,
                    'economicalCode' => $economicalCode,
                    'isExpense' => true,
                    'isOrdinary' => false
            ]),
            'numArticlesRevenuesExtraordinary' => $em->getRepository(Article::class)
                ->countWithRecursiveCode([
                    'budget' => $budget,
                    'functionalCode' => $functionalCode,
                    'economicalCode' => $economicalCode,
                    'isExpense' => false,
                    'isOrdinary' => false
            ])
        ]);
    }
}
