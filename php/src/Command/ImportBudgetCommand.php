<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use League\Csv\Reader;
use App\Import\FunctionalCodeHelper;
use App\Import\EconomicalCodeHelper;
use App\Import\ProjectHelper;
use App\Entity\FunctionalCode;
use App\Entity\EconomicalCode;
use App\Entity\Budget;
use App\Entity\Entity;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Article;

class ImportBudgetCommand extends Command
{
    protected static $defaultName = 'app:import-budget';
    
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    protected $entity;
    
    protected $budget;
    
    protected $rootEconomicalCode;
    
    protected $rootFunctionalCode;
    
    /**
     *
     * @var FunctionalCodeHelper
     */
    protected $functionalCodeHelper;
    
    /**
     *
     * @var EconomicalCodeHelper;
     */
    protected $economicalCodeHelper;
    
    /**
     *
     * @var ProjectHelper
     */
    protected $projectHelper;
    
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct(self::$defaultName);
    }

        protected function configure()
    {
        $this
            ->setDescription('Import a budget')
            ->addArgument('file', InputArgument::REQUIRED, 'Path to csv file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $path = $input->getArgument('file');
        $this->bootImport();
        
        $reader = Reader::createFromPath($path, 'r');
        $reader->setHeaderOffset(0);
        
        foreach ($reader->getRecords() as $offset => $row) {
            
            $output->writeln('Handling row '.$offset);
            
            $this->insertArticle($row);
            
            if ($offset % 50 === 0) {
                $this->em->flush();
                $this->em->clear(Article::class);
            }
            
            if ($offset % 250 === 0) {
                $this->em->clear();
                $this->bootImport();
            }
        }
        
        $this->em->flush();

        $io->success('Import finished');
    }
    
    protected function bootImport()
    {
        $budget = $this->budget = $this->em->getRepository(Budget::class)
            ->find(1000);
        $rootEconomicalCode = $this->rootEconomicalCode = $this->em
            ->getRepository(EconomicalCode::class)->find(1);
        $rootFunctionalCode = $this->rootFunctionalCode = $this->em
            ->getRepository(FunctionalCode::class)->find(1);
        $this->entity = $this->em->getRepository(Entity::class)->find(1);
        $this->functionalCodeHelper = new FunctionalCodeHelper($this->em, $rootFunctionalCode);
        $this->economicalCodeHelper = new EconomicalCodeHelper($this->em, $rootEconomicalCode);
        $this->projectHelper = new ProjectHelper($this->em);
    }
    
    protected function insertArticle($row)
    {
        $article = new Article();
        $article
            ->setLabel($row['Libellé_Article_1'])
            ->setIndice($row['Indice'])
            ->setEconomicalCode($this->economicalCodeHelper->getOrCreate($row['Econ']))
            ->setFunctionalCode($this->functionalCodeHelper->getOrCreate($row['Fonct']))
            ->setAmount((float) $row['B 2012'])
            ->setIsExpense($article->getEconomicalCode()->isExpense())
            ->setIsOrdinary($article->getEconomicalCode()->isOrdinary())
            ->setBudget($this->budget)
            ;
        
        if (FALSE === empty($row['nproj'])) {
            $article
                ->setProject($this->projectHelper
                    ->getOrCreate($row['nproj'], $row['Projet'], $this->entity));
        }
        
        $this->em->persist($article);
    }
}
