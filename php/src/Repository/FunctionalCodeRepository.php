<?php

namespace App\Repository;

use App\Entity\FunctionalCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\DBAL\Types\Type;

/**
 * @method FunctionalCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method FunctionalCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method FunctionalCode[]    findAll()
 * @method FunctionalCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FunctionalCodeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FunctionalCode::class);
    }

    
    public function findByRoot(FunctionalCode $root, $onlyId = true)
    {
        $sql = 'WITH RECURSIVE q AS (
            SELECT id, parent_id FROM app.functional_code WHERE id = :rootid 
            UNION 
            SELECT functional_code.id, functional_code.parent_id
            FROM app.functional_code
            INNER JOIN q ON q.id = functional_code.parent_id)
            select id from q'
            ;
        
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id', Type::INTEGER);
        
        return \array_map(function($r) { return $r['id'];}, 
            $this->_em->createNativeQuery($sql, $rsm)
                ->setParameter('rootid', $root->getId())
                ->getResult());
    }
}
