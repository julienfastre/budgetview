<?php

namespace App\Repository;

use App\Entity\EconomicalCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\DBAL\Types\Type;

/**
 * @method EconomicalCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method EconomicalCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method EconomicalCode[]    findAll()
 * @method EconomicalCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EconomicalCodeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EconomicalCode::class);
    }
    
    public function findByRoot(EconomicalCode $root, $onlyId = true)
    {
        $sql = 'WITH RECURSIVE q AS (
            SELECT id, parent_id FROM app.economical_code WHERE id = :rootid
            UNION 
            SELECT economical_code.id, economical_code.parent_id
            FROM app.economical_code
            INNER JOIN q ON q.id = economical_code.parent_id)
            select id from q'
            ;
        
        //$sql = 'select id from app.economical_code WHERE id = :rootid';
        
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id', Type::INTEGER);
        
        return \array_map(function($row) { return $row['id']; }, $this->_em->createNativeQuery($sql, $rsm)
            ->setParameter('rootid', $root->getId())
            ->getResult());
    }
}
