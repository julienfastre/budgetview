<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Repository\EconomicalCodeRepository;
use App\Repository\FunctionalCodeRepository;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    /**
     *
     * @var FunctionalCodeRepository
     */
    protected $functionalCodeRepository;
    
    /**
     *
     * @var EconomicalCodeRepository
     */
    protected $economicalCodeRepository;
    
    public function __construct(
        RegistryInterface $registry,
        EconomicalCodeRepository $economicalCodeRepository,
        FunctionalCodeRepository $functionalCodeRepository)
    {
        parent::__construct($registry, Article::class);
        $this->economicalCodeRepository = $economicalCodeRepository;
        $this->functionalCodeRepository = $functionalCodeRepository;
    }
    
    public function findByWithRecursiveCode(
        array $criteria, 
        $orderBy = null, 
        $limit = null, 
        $offset = null
    ) {
        return $this->findBy($this->makeCriteriaCodeRecursive($criteria), 
            $orderBy, $limit, $offset);
    }
    
    public function countWithRecursiveCode(array $criteria): int
    {
        return $this->count($this->makeCriteriaCodeRecursive($criteria));
    }
    
    protected function makeCriteriaCodeRecursive($criteria)
    {
        if (\array_key_exists('functionalCode', $criteria)) {
            $criteria['functionalCode'] = $this->functionalCodeRepository
                ->findByRoot($criteria['functionalCode']);
        }
        
        if (\array_key_exists('economicalCode', $criteria)) {
            $criteria['economicalCode'] =$this->economicalCodeRepository
                ->findByRoot($criteria['economicalCode']);
        }
        
        return $criteria;
    }

//    /**
//     * @return Article[] Returns an array of Article objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
