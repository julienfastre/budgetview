<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EntityRepository")
 * @ORM\Table(name="app.entity", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="entity_slug_idx", columns={"slug"})
 * })
 */
class Entity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $label;
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $slug;
    
    /**
     * @ORM\OneToMany(targetEntity="Entity", mappedBy="entity")
     */
    private $budgets;
    
    public function __construct()
    {
        $this->budgets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }
    
    public function getSlug(): string
    {
        return $this->slug;
    }
    
    public function getBudgets(): Doctrine\Common\Collections\Collection
    {
        return $this->budgets;
    }
}
