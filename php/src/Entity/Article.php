<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @ORM\Table(name="app.article",
 *  indexes={
 *      @ORM\Index(name="article_filtering_economical_code", columns={"budget_id", 
 *          "is_expense", "economical_code_id"}),
 *      @ORM\Index(name="article_filtering_functional_code", columns={"budget_id", 
 *          "is_expense", "functional_code_id"}),
 *  })
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $label;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=2)
     */
    private $amount;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isExpense;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EconomicalCode")
     * @ORM\JoinColumn(nullable=false)
     */
    private $economicalCode;

    /**
     * @var App\Entity\FunctionalCode
     * @ORM\ManyToOne(targetEntity="App\Entity\FunctionalCode")
     * @ORM\JoinColumn(nullable=false)
     */
    private $functionalCode;
    
    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $indice;

    /**
     * @var \App\Entity\Budget
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Budget")
     * @ORM\JoinColumn(nullable=false)
     */
    private $budget;
    
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isOrdinary = true;
    
    /**
     * @var Project|null
     * 
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(nullable=true)
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getIsExpense(): ?bool
    {
        return $this->isExpense;
    }

    public function setIsExpense(bool $isExpense): self
    {
        $this->isExpense = $isExpense;

        return $this;
    }
    
    public function setIsOrdinary(bool $isOrdinary): self
    {
        $this->isOrdinary = $isOrdinary;
        
        return $this;
    }
    
    public function getIsOrdinary(): bool
    {
        return $this->isOrdinary;
    }

    public function getEconomicalCode(): ?EconomicalCode
    {
        return $this->economicalCode;
    }

    public function setEconomicalCode(?EconomicalCode $economicalCode): self
    {
        $this->economicalCode = $economicalCode;

        return $this;
    }

    public function getFunctionalCode(): ?FunctionalCode
    {
        return $this->functionalCode;
    }

    public function setFunctionalCode(?FunctionalCode $functionalCode): self
    {
        $this->functionalCode = $functionalCode;

        return $this;
    }

    public function getBudget(): ?Budget
    {
        return $this->budget;
    }

    public function setBudget(?Budget $budget): self
    {
        $this->budget = $budget;

        return $this;
    }
    
    public function setProject(?Project $project): self
    {
        $this->project = $project;
        
        return $this;
    }
    
    public function getProject(): ?Project
    {
        return $this->project;
    }
    
    public function setIndice(int $indice): self
    {
        $this->indice = $indice;
        
        return $this;
    }
    
    public function getIndice(): int
    {
        return $this->indice;
    }
}
