<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EconomicalCodeRepository")
 * @ORM\Table(name="app.economical_code")
 */
class EconomicalCode implements CodeInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $code = '';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EconomicalCode", inversedBy="chidren")
     */
    private $parent;
    
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EconomicalCode", mappedBy="parent")
     */
    private $children;
    
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }
    
    public function hasParent(): bool
    {
        return $this->parent instanceof self;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }
    
    public function getChildren(): \Doctrine\Common\Collections\Collection
    {
        $criteria = Criteria::create();
        $criteria->orderBy(['code' => 'ASC']);
        
        
        return $this->children->matching($criteria);
    }
    
    public function isAbstract(): bool
    {
        return \strlen($this->getCode()) <= 3;
    }
    
    public function isExpense(): bool
    {
        if ($this->isAbstract()) {
            throw new \LogicalException("You should not call is expense on an "
                . "abstract code");
        }
        
        return \in_array($this->getCode()[1], ['0', '1', '2', '3', '4']);
    }
    
    public function isExtraordinary(): bool
    {
        if ($this->isAbstract()) {
            throw new \LogicalException("You should not call is expense on an "
                . "abstract code");
        }
        
        return \in_array($this->getCode()[0], ['5', '6', '7', '8', '9']);
    }
    
    public function isOrdinary(): bool
    {
        return FALSE === $this->isExtraordinary();
    }
    
    public function __toString(): string
    {
        if (empty($this->getCode())) {
            // root - we do not return code
            return $this->getLabel();
        }
        
        return $this->getCode().' - '.$this->getLabel();
    }
        
}
