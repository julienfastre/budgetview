<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FunctionalCodeRepository")
 * @ORM\Table(name="app.functional_code")
 */
class FunctionalCode implements CodeInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $label;
    
    /**
     * @ORM\Column(type="string", length=6)
     */
    private $code;
    
    /**
     * @ORM\ManyToOne(
     *  targetEntity="FunctionalCode",
     *  inversedBy="chidren"
     * )
     */
    private $parent;
    
    /**
     * @ORM\OneToMany(
     *  targetEntity="FunctionalCode",
     *  mappedBy="parent"
     * )
     */
    private $children;
    
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }
    
    public function getChildren(): \Doctrine\Common\Collections\Collection
    {
        $criteria = Criteria::create();
        $criteria->orderBy(['code' => 'ASC']);
        
        return $this->children->matching($criteria);
    }
    
    public function setParent(FunctionalCode $code): self
    {
        $this->parent = $code;
        
        return $this;
    }
    
    public function getParent(): ?CodeInterface
    {
        return $this->parent;
    }
    
    public function hasParent(): bool
    {
        return $this->parent instanceof self;
    }
    
    public function setCode($code): self
    {
        $this->code = $code;
        
        return $this;
    }
    
    public function getCode(): ?string
    {
        return $this->code;
    }
    
    public function __toString(): string
    {
        if (empty($this->getCode())) {
            // root - we do not return code
            return $this->getLabel();
        }
        
        return $this->getCode().' - '.$this->getLabel();
    }
}
