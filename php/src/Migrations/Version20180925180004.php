<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180925180004 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA app');
        $this->addSql('CREATE SEQUENCE app.article_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE app.budget_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE app.functional_code_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE app.economical_code_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE app.project_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE app.entity_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE app.article (id INT NOT NULL, economical_code_id INT NOT NULL, functional_code_id INT NOT NULL, budget_id INT NOT NULL, project_id INT DEFAULT NULL, label TEXT NOT NULL, amount NUMERIC(10, 2) NOT NULL, is_expense BOOLEAN NOT NULL, indice INT NOT NULL, is_ordinary BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8F9D1F2512A27F9 ON app.article (economical_code_id)');
        $this->addSql('CREATE INDEX IDX_8F9D1F25C435B283 ON app.article (functional_code_id)');
        $this->addSql('CREATE INDEX IDX_8F9D1F2536ABA6B8 ON app.article (budget_id)');
        $this->addSql('CREATE INDEX IDX_8F9D1F25166D1F9C ON app.article (project_id)');
        $this->addSql('CREATE TABLE app.budget (id INT NOT NULL, entity_id INT DEFAULT NULL, year INT NOT NULL, type VARCHAR(50) NOT NULL, version VARCHAR(50) NOT NULL, date DATE NOT NULL, label TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DCEE416481257D5D ON app.budget (entity_id)');
        $this->addSql('CREATE TABLE app.functional_code (id INT NOT NULL, parent_id INT DEFAULT NULL, label TEXT NOT NULL, code VARCHAR(6) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C281ED10727ACA70 ON app.functional_code (parent_id)');
        $this->addSql('CREATE TABLE app.economical_code (id INT NOT NULL, parent_id INT DEFAULT NULL, label TEXT NOT NULL, code VARCHAR(6) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6296A776727ACA70 ON app.economical_code (parent_id)');
        $this->addSql('CREATE TABLE app.project (id INT NOT NULL, entity_id INT NOT NULL, number VARCHAR(150) NOT NULL, label TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A214C1AD81257D5D ON app.project (entity_id)');
        $this->addSql('CREATE TABLE app.entity (id INT NOT NULL, label TEXT NOT NULL, slug VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE app.article ADD CONSTRAINT FK_8F9D1F2512A27F9 FOREIGN KEY (economical_code_id) REFERENCES app.economical_code (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE app.article ADD CONSTRAINT FK_8F9D1F25C435B283 FOREIGN KEY (functional_code_id) REFERENCES app.functional_code (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE app.article ADD CONSTRAINT FK_8F9D1F2536ABA6B8 FOREIGN KEY (budget_id) REFERENCES app.budget (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE app.article ADD CONSTRAINT FK_8F9D1F25166D1F9C FOREIGN KEY (project_id) REFERENCES app.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE app.budget ADD CONSTRAINT FK_DCEE416481257D5D FOREIGN KEY (entity_id) REFERENCES app.entity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE app.functional_code ADD CONSTRAINT FK_C281ED10727ACA70 FOREIGN KEY (parent_id) REFERENCES app.functional_code (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE app.economical_code ADD CONSTRAINT FK_6296A776727ACA70 FOREIGN KEY (parent_id) REFERENCES app.economical_code (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE app.project ADD CONSTRAINT FK_A214C1AD81257D5D FOREIGN KEY (entity_id) REFERENCES app.entity (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE app.article DROP CONSTRAINT FK_8F9D1F2536ABA6B8');
        $this->addSql('ALTER TABLE app.article DROP CONSTRAINT FK_8F9D1F25C435B283');
        $this->addSql('ALTER TABLE app.functional_code DROP CONSTRAINT FK_C281ED10727ACA70');
        $this->addSql('ALTER TABLE app.article DROP CONSTRAINT FK_8F9D1F2512A27F9');
        $this->addSql('ALTER TABLE app.economical_code DROP CONSTRAINT FK_6296A776727ACA70');
        $this->addSql('ALTER TABLE app.article DROP CONSTRAINT FK_8F9D1F25166D1F9C');
        $this->addSql('ALTER TABLE app.budget DROP CONSTRAINT FK_DCEE416481257D5D');
        $this->addSql('ALTER TABLE app.project DROP CONSTRAINT FK_A214C1AD81257D5D');
        $this->addSql('DROP SEQUENCE app.article_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE app.budget_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE app.functional_code_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE app.economical_code_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE app.project_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE app.entity_id_seq CASCADE');
        $this->addSql('DROP TABLE app.article');
        $this->addSql('DROP TABLE app.budget');
        $this->addSql('DROP TABLE app.functional_code');
        $this->addSql('DROP TABLE app.economical_code');
        $this->addSql('DROP TABLE app.project');
        $this->addSql('DROP TABLE app.entity');
    }
}
