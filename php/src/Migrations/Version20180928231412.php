<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add indexes for filtering amount by functional code, budget, economical code,
 * is_expense
 */
final class Version20180928231412 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE INDEX article_filtering_economical_code ON app.article (budget_id, is_expense, economical_code_id)');
        $this->addSql('CREATE INDEX article_filtering_functional_code ON app.article (budget_id, is_expense, functional_code_id)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX article_filtering_economical_code');
        $this->addSql('DROP INDEX article_filtering_functional_code');
    }
}
