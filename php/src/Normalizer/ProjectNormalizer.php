<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace App\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use App\Entity\Project;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ProjectNormalizer implements NormalizerInterface
{
    /**
     * 
     * @param Project $project
     * @param string $format
     * @param array $context
     */
    public function normalize($project, $format = null, array $context = array())
    {
        return [
            'id' => $project->getId(),
            'number' => $project->getNumber(),
            'label' => $project->getLabel()
        ];
    }

    public function supportsNormalization($data, $format = null): bool
    {
        return $format === 'json' && $data instanceof Project;
    }
}
