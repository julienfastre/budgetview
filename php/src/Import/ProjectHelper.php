<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace App\Import;

use App\Entity\Project;
use App\Entity\Entity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NoResultException;

/**
 * 
 *
 * 
 */
class ProjectHelper
{
    /**
     *
     * @var EntityManagerInterface
     */
    protected $em;
    
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    
    public function getOrCreate($number, $label, Entity $entity)
    {
        try {
            return $this->em->getRepository(Project::class)
                ->findOneBy([
                    'entity' => $entity,
                    'number' => $number
                ]);
        } catch (NoResultException $ex) {
            $p = (new Project())
                ->setNumber($number)
                ->setLabel($label)
                ->setEntity($entity)
                ;
            
            $this->em->persist($p);
            
            return $p;
        }
    }
}
