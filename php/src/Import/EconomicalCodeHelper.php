<?php
/*
 * Copyright (C) 2018 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace App\Import;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\EconomicalCode;
use Doctrine\ORM\NoResultException;

/**
 * Create or import economical code
 */
class EconomicalCodeHelper
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;
    
    /**
     *
     * @var int[]
     */
    private $knownChildrenIds = array();
    
    /**
     *
     * @var EconomicalCode
     */
    private $root;
    
    function __construct(EntityManagerInterface $em, EconomicalCode $root)
    {
        $this->em = $em;
        $this->root = $root;
        /* @var $repository Doctrine\ORM\Repository */
        $repository = $this->em->getRepository(EconomicalCode::class);
        $this->knownChildrenIds = $repository->findByRoot($root);
    }

    
    public function getOrCreate($code): EconomicalCode
    {
        /* @var $repository Doctrine\ORM\Repository */
        $repository = $this->em->getRepository(EconomicalCode::class);

        $r = $repository->findOneBy([
            'code' => $code,
            'id' => $this->knownChildrenIds
        ]);

        if ($r instanceof EconomicalCode) {
            return $r;
        }

        $ecCode = (new EconomicalCode())
            ->setParent($this->getOrCreateParent($code))
            ->setCode($code)
            ->setLabel($code);
        $this->em->persist($ecCode);
        $this->em->flush();
        $this->knownChildrenIds[] = $ecCode->getId();

        return $ecCode;
    }
    
    protected function getOrCreateParent($childrenCode): EconomicalCode
    {
        if (\strlen($childrenCode) === 1) {
            return $this->root;
        }
        
        $code = $this->makeParentCode($childrenCode);
        
        return $this->getOrCreate($code);
    }
    
    /**
     * 
     * @param string $code
     * @return string
     */
    protected function makeParentCode(string $code) {
        if (strlen($code) === 5) {
            return \substr($code, 0, 3);
        }
        
        $len = strlen($code);
        
        return \substr($code, 0, $len-1);
    }
}
