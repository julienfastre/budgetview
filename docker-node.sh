#!/bin/bash

# exécute toujours dans le répertoire `php`
cd $(dirname $0)

if [ $# -eq 0 ]
then
   cmd=bash
else
   cmd="${@}"
fi

docker run --rm -it --user $(id -u):$(id -g) -v ${PWD}/front:/app --workdir /app -e YARN_CACHE_FOLDER=/app/.yarncache node:9 ${cmd}
