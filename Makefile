



help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  build:           prepare les images"

build:
	./docker-node.sh yarn run encore production
	cp front/build/manifest.json php/manifest.json
	docker-compose build --pull php nginx

push-container:
	docker push registry.gitlab.com/champs-libres/blog.champs-libres.coop/blog:latest

