require("./index.scss");

var initListArticle = (container) => {
    var zone = {
        container: container,
        listWrapper: document.createElement('div'),
        numArticles: container.dataset.numArticles,
        loadPath: container.dataset.loadPath,
        loadMore: document.createElement('button'),
        offset: 0
    };
    
    zone.container.classList.add('article-list');
    zone.listWrapper.classList.add('article-list__list');
    container.appendChild(zone.listWrapper);
    makeHeaders(zone);
    makeLoadButton(zone);
    loadArticles(zone);
};

var loadArticles = (zone) => {
    var query = zone.loadPath+'?limit='+encodeURIComponent(25)
        + '&offset='+encodeURIComponent(zone.offset);
    
    window.fetch(query).then((r) => {
        if (r.ok) {
            return r.json();
        }
    }).then((data) => {
        for (let i=0; i < data.length; i++) {
            makeRowArticle(data[i], zone);
        }
        zone.offset = zone.offset + data.length;
        
        if (zone.offset >= zone.numArticles) {
            zone.loadMore.remove();
        }
    });
};

var makeRowArticle = (data, zone) => {
    var
        row = createRow(),
        cellLabel = createCell(null), 
        cellAmount = createCell(null), 
        label = document.createTextNode(data.label), 
        amount = document.createTextNode(new Intl.NumberFormat('be-BE', { style: 'currency', currency: 'EUR' }).format(data.amount))
        ;
        
    cellLabel.classList.add('article-list__list__row__cell--title');
    cellAmount.classList.add('article-list__list__row__cell--amount');
    cellLabel.appendChild(label);
    cellAmount.appendChild(amount);
    row.appendChild(cellLabel);
    row.appendChild(cellAmount);
    zone.listWrapper.appendChild(row);
};

var makeHeaders = (zone) => {
    var 
        row = createRow(),
        cellLabel = createCell(document.createTextNode('Titre')),
        cellAmount = createCell(document.createTextNode('Montant'))
        ;
    
    cellLabel.classList.add('article-list__list__row__cell--title');
    cellAmount.classList.add('article-list__list__row__cell--amount');
    row.classList.add('article-list__list__row--header');
    row.appendChild(cellLabel);
    row.appendChild(cellAmount);
    zone.listWrapper.appendChild(row);
};

var makeLoadButton = (zone) => {
    zone.loadMore.appendChild(document.createTextNode("Voir plus..."));
    zone.container.appendChild(zone.loadMore);
    zone.loadMore.addEventListener('click', () => loadArticles(zone));
};

var createRow = () => {
    var row = document.createElement('div');
    row.classList.add('article-list__list__row');
    
    return row;
};

var createCell = (child) => {
    var cell = document.createElement('div');
    cell.classList.add('article-list__list__row__cell');
    console.log(child);
    if (null !== child) {
        cell.appendChild(child);
    }
    
    return cell;
};

window.addEventListener('load', () => {
    var 
        containers = document.querySelectorAll('[data-list-articles]');
        
    for (let i=0; i < containers.length; i++) {
        initListArticle(containers[i]);
    }
});
