
import Chart from 'chart.js';

var initGraph = () => {
    var 
        ctx = document.querySelector('#byFunction'),
        data = JSON.parse(ctx.dataset.repartition),
        revenues = data.sumRevenues,
        expenses = [],
        chart
        ;
    
    console.log('data', data);
    // inverting expenses
    data.sumExpenses.forEach(function(el) {
        expenses.push(el * -1);
    });
    chart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: data.labels,
            datasets: [
            // expenses
            {
                label: 'Dépenses',
                data: expenses,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            },
            // revenues
            {
                label: 'Revenus',
                data: revenues
            }]
        },
        options: {
            tooltips: {
                callbacks: {
                    /*label: function(tooltipItem, data) {
                        var sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        
                        console.log('sum', sum);
                        
                        
                    }*/
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    },
                    stack: true
                }]
            }
        }
    });
    
};

window.addEventListener('load', function(e) {
    initGraph();
});
