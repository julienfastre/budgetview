
import Chart from 'chart.js';

var initGraph = () => {
    var 
        ctxExpenses = document.querySelector('#byNatureExpenses'),
        ctxRevenues = document.querySelector('#byNatureRevenues'),
        dataExpenses = JSON.parse(ctxExpenses.dataset.repartition),
        dataRevenues = JSON.parse(ctxRevenues.dataset.repartition),
        chartExp, chartRev
        ;

    chartExp = new Chart(ctxExpenses, {
        type: 'pie',
        data: {
            labels: dataExpenses.labels,
            datasets: [
            // expenses
            {
                label: 'Répartition des dépenses',
                data: dataExpenses.sum,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ]
        },
        options: {
            tooltips: {
                callbacks: {
                    /*label: function(tooltipItem, data) {
                        var sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        
                        console.log('sum', sum);
                        
                        
                    }*/
                }
            }
        }
    });
    
    chartRev = new Chart(ctxRevenues, {
        type: 'pie',
        data: {
            labels: dataExpenses.labels,
            datasets: [
            // expenses
            {
                label: 'Répartition des revenus',
                data: dataRevenues.sum,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }
        ]
        },
        options: {
            tooltips: {
                callbacks: {
                    /*label: function(tooltipItem, data) {
                        var sum = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        
                        console.log('sum', sum);
                        
                        
                    }*/
                }
            }
        }
    });
    
};

window.addEventListener('load', function(e) {
    initGraph();
});

